<?php

declare(strict_types=1);

namespace console\controllers\elastic;

use Yii;
use yii\console\Controller;

class ExampleController extends Controller
{
    public function actionIndex(): void
    {
        echo var_export(Yii::$app->elasticsearch->get('yii/_search'), true);
    }
}
