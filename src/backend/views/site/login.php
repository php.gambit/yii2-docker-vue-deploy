<?php

declare(strict_types=1);

/** @var yii\web\View $this */
/* @var yii\bootstrap4\ActiveForm $form */
/* @var \common\models\LoginForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Login';
?>
<div class="site-login">
    <div class="mt-5 offset-lg-3 col-lg-6">
        <h1><?php echo Html::encode($this->title); ?></h1>

        <p>Please fill out the following fields to login:</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?php echo $form->field($model, 'username')->textInput(['autofocus' => true]); ?>

            <?php echo $form->field($model, 'password')->passwordInput(); ?>

            <?php echo $form->field($model, 'rememberMe')->checkbox(); ?>

            <div class="form-group">
                <?php echo Html::submitButton('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']); ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
