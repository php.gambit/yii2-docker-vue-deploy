<?php

declare(strict_types=1);

use yii\console\Application;
use yii\web\User;

/**
 * @var yii\elasticsearch\Connection $elasticsearch
 */
class Yii
{
    /**
     *
     * @var \yii\web\Application|Application|__Application
     */
    public static $app;
}

/**
 * @property yii\rbac\DbManager $authManager
 * @property User|__WebUser     $user
 */
class __Application
{
}

/**
 * @property app\models\User $identity
 */
class __WebUser
{
}
