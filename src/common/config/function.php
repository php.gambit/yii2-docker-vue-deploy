<?php

declare(strict_types=1);

function loadConfig(string $path, bool $isReq = false)
{
    if (!file_exists($path) && !$isReq) {
        return [];
    }

    require $path;
}
