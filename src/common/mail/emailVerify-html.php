<?php

declare(strict_types=1);

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\User $user */
$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
<div class="verify-email">
    <p>Hello <?php echo Html::encode($user->username); ?>,</p>

    <p>Follow the link below to verify your email:</p>

    <p><?php echo Html::a(Html::encode($verifyLink), $verifyLink); ?></p>
</div>
