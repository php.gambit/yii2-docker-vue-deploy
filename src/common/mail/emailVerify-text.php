<?php

declare(strict_types=1);

/** @var yii\web\View $this */

/** @var common\models\User $user */
$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
Hello <?php echo $user->username; ?>,

Follow the link below to verify your email:

<?php echo $verifyLink; ?>
