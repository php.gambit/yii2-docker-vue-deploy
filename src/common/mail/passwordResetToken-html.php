<?php

declare(strict_types=1);

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\User $user */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hello <?php echo Html::encode($user->username); ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?php echo Html::a(Html::encode($resetLink), $resetLink); ?></p>
</div>
