<?php

declare(strict_types=1);

/** @var yii\web\View $this */

/** @var common\models\User $user */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hello <?php echo $user->username; ?>,

Follow the link below to reset your password:

<?php echo $resetLink; ?>
