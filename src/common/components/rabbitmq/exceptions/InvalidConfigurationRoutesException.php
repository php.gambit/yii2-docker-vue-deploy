<?php

declare(strict_types=1);

namespace common\components\rabbitmq\exceptions;

use common\exceptions\AbstractException;

class InvalidConfigurationRoutesException extends AbstractException
{
}
