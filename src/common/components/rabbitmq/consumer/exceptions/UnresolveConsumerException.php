<?php

declare(strict_types=1);

namespace common\components\rabbitmq\consumer\exceptions;

use common\exceptions\AbstractException;

class UnresolveConsumerException extends AbstractException
{
}
