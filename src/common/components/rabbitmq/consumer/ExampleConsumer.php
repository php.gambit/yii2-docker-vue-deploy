<?php

declare(strict_types=1);

namespace common\components\rabbitmq\consumer;

use common\components\rabbitmq\consumer\interfaces\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class ExampleConsumer implements ConsumerInterface
{
    /**
     * @param AMQPMessage $msg
     *
     * @return bool
     */
    public function execute(AMQPMessage $message)
    {
        $message->ack(true);
    }
}
