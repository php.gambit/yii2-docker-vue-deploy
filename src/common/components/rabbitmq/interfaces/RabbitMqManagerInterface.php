<?php

declare(strict_types=1);

namespace common\components\rabbitmq\interfaces;

use common\components\rabbitmq\dto\ConsumeDto;
use common\components\rabbitmq\dto\publish\PublishDto;

interface RabbitMqManagerInterface
{
    public function consume(ConsumeDto $data): bool;

    public function publish(PublishDto $publishData): bool;
}
