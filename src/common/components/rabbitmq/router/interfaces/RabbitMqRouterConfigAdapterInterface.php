<?php

declare(strict_types=1);

namespace common\components\rabbitmq\router\interfaces;

interface RabbitMqRouterConfigAdapterInterface
{
    public function getConfig(): array;
}
