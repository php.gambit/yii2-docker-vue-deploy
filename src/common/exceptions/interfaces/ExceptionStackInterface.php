<?php

declare(strict_types=1);

namespace common\exceptions\interfaces;

interface ExceptionStackInterface
{
    public function getCategory(): string;
}
